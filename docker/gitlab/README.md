### 如何使用Gitlab CI來達到自動化測試與佈署

* [安裝gitlab runner on docker service](https://docs.gitlab.com/runner/install/docker.html)
* 註冊  
` docker run --rm -t -i -v /path/to/config:/etc/gitlab-runner --name gitlab-runner gitlab/gitlab-runner register`
