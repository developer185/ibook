* [For gitlab 部屬](gitlab/README.md)

```
docker run -it --rm -p 5901:5901 redsigma/ubuntu-vbox \
bash -c "vncserver :1 -geometry 1280x800 -depth 24 && tail -F /root/.vnc/*.log"
```

# Remove containers using more than one filter
`docker ps -a -f status=exited -f status=created`

`docker rm $(docker ps -a -f status=exited -f status=created)`

# Run Jenkins
```
docker run \
    --name jenkins \
    -d --restart always \
    -p 8080:8080 -p 50000:50000 \
    -v /d/sigma/srv_docker/jenkins:/var/jenkins_home \
    jenkins/jenkins:lts
```



