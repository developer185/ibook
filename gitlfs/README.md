# git 常用指令

##### if you want to list files of the current branch  
- `git ls-tree -r HEAD --name-only`  

##### if you want to delete remote branch  
- `git branch -r -d origin/branch-name`  
- `git push origin :branch-name`  

#### if after "git add", but need add a gitignore folder/file, this file can't ignore, How to do?  
-   command   
  
	git rm -r --cached .
	#add again
	git add  .
	git commit -m "fixed untracked files"

# [3.1 Git LFS](git-lfs.md)

# reference
[1](http://carlislebear.blogspot.com/2019/02/gitlab-jenkins-continuous-integration.html)