# 1.1  Git LFS

## Git LFS  Server install 
	https://github.com/artemkin/git-lfs-server

##### apc042 
* Install location on /home/users/ideast/srv/lfs_server
* RUN ./lfs_server.sh -verbose -s apc042 -p 9999

 
## Git LFS  install (Git version > 1.8.5)

    https://github.com/git-lfs/git-lfs/releases

 1. Linux


    curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
    sudo apt-get install git-lfs
    git lfs install
    (git lfs install --skip-smudge)
    
 2. Mac
*  安装HomeBrew
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew install git-lfs
    git lfs install
 3. Windows
	download windows installer
	執行 windows installer後
	command line run

    git lfs install


## How to use
执行 git lfs install 开启lfs功能
### git lfs track 進行追蹤

    example :
    git lfs track "*.png" 

### git lfs track 查看現有文件
### gitattributes  它保存了文件的追蹤紀錄
### git lfs ls-files 可以顯示文件列表
push 到remote ，LFS 跟踪的文件會以『Git LFS』的形式顯示
### clone 時用'git clone' 或 'git lfs clone'均可
